//
//  AppDelegate.h
//  Socket
//
//  Created by yan on 2016/12/8.
//  Copyright © 2016年 yan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

